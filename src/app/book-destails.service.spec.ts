import { TestBed, inject } from '@angular/core/testing';

import { BookDestailsService } from './book-destails.service';

describe('BookDestailsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BookDestailsService]
    });
  });

  it('should be created', inject([BookDestailsService], (service: BookDestailsService) => {
    expect(service).toBeTruthy();
  }));
});
