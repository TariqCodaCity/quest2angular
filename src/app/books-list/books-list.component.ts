import { Component, OnInit } from '@angular/core';
import { BookService } from '../book.service';
@Component({
  selector: 'app-books-list',
  templateUrl: './books-list.component.html',
  styleUrls: ['./books-list.component.css']
})
export class BooksListComponent implements OnInit {
  public books = [];

  constructor(private _bookService: BookService) {   }

  ngOnInit() {
    this._bookService.getBooks()
    .subscribe(data => this.books = data);
  }

}
