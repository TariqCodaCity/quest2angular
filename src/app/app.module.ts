import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { BooksListComponent } from './books-list/books-list.component';
import { BookService } from './book.service';
import {HttpClientModule} from '@angular/common/http';
import { BookDetailsComponent } from './book-details/book-details.component';
import { BookDestailsService } from './book-destails.service';


@NgModule({
  declarations: [
    AppComponent,
    BooksListComponent,
    BookDetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [BookService,BookDestailsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
