export interface IBookDetails {
    bookingId: number;
    name: string ;
    mobile: string;
    bookedDate: Date;
    email: string;
    note: string;
    isDeleted: boolean;
    timeSlotId: number;
    treatmentId: number;
    timeSlotTitle: string;
}
