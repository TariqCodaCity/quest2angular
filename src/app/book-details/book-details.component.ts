import { Component, OnInit } from '@angular/core';
import { BookDestailsService } from '../book-destails.service';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {
  public book;
  constructor(private _bookDetails: BookDestailsService) { }

  ngOnInit() {
    this._bookDetails.getBookDetails()
    .subscribe(data => this.book = data);
    console.log("here  ",this.book);
    
  }

}
