export interface IBook {
    bookingId: number;
    name: string ;
    mobile: string;
    bookedDate: Date;
    email: string;
    note: string;
    isDeleted: boolean;
    timeSlotId: number;
    treatmentId: number;
}
