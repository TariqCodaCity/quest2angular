import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IBookDetails } from './IBookDetails';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class BookDestailsService {


  constructor(private http: HttpClient) { }
  private _getBookDetailsUrl = 'http://localhost:62659/api/books/9';
  getBookDetails(): Observable<IBookDetails[]> {
    return this.http.get<IBookDetails[]>(this._getBookDetailsUrl);
}
}
