import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IBook } from './IBook';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class BookService {
  constructor(private http: HttpClient) { }

  private _getBooksUrl = 'http://localhost:62659/api/books';
  getBooks(): Observable<IBook[]> {
    return this.http.get<IBook[]>(this._getBooksUrl);
  }
}
